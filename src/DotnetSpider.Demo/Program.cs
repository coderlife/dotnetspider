﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DotnetSpider.Demo.Entity;
using System.Data;
using System.Data.SqlClient;

namespace DotnetSpider.Demo
{
    public class Program
    {
        public static void Main(string[] args)
        {
            SqlConnection conn = new SqlConnection();
           if (args[0]!=null && args[1]!=null)
            {
                int start = 0;
                int end = 0;
                if (int.TryParse(args[0],out start) &&(int.TryParse(args[1],out end)))
                {
                    Console.WriteLine(args[0] + "," + args[1]);
                    Avmoo.Run(start, end);
                }
            }
           else
            {
                Avmoo.Run(1,1);

            }
        }
    }
}
